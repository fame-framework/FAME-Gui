# Request description
(Summarize the feature you request)

# Use case
(Explain how this would affect your experience of FAME-Gui)

# Supporting images or sketches
(If possible, please provide some screenshots or sketches demonstrating the feature you request)

# Other
(Feel free to add any information you think is relevant in order to adress this request)
