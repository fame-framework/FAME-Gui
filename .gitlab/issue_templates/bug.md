# Environment
- fameio: `x.x.x`
- python: `x.x.x`
- OS: `Win10`

# Bug description
(Summarize the bug encountered concisely)

# Steps to reproduce
1. (How one can reproduce the issue - this is very important)
2. 

## Expected result:
(What you should see)

## Actual result: 
(What actually happens)

# Screenshots
(If possible, please provide some screenshots demonstrating the bug)

# Other
(Feel free to add any information you think is relevant in order to adress this issue)
